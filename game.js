/*

Created by:
JIN HheHe

*/
var player;
var Spikes = [];
var gl;

var display, scoreDisplay, statusDisplay, playDisplay, levelDisplay;

var score = 200,
	scoreThreshold = 0;
var level = 1;
var SetTime = 60;
var gameStarted = false;
var lastFrame = performance.now();
var demo = true;

var semuaKey = [37, 38, 39, 40];
var event = ['keyup', 'keydown'];

var evtDown38 = new KeyboardEvent("keydown", { keyCode: 38 });
var evtDown39 = new KeyboardEvent("keydown", { keyCode: 39 });
var evtDown40 = new KeyboardEvent("keydown", { keyCode: 40 });
var evtDown37 = new KeyboardEvent("keydown", { keyCode: 37 });
var evtUp38 = new KeyboardEvent("keyup", { keyCode: 38 });
var evtUp39 = new KeyboardEvent("keyup", { keyCode: 39 });
var evtUp40 = new KeyboardEvent("keyup", { keyCode: 40 });
var evtUp37 = new KeyboardEvent("keyup", { keyCode: 37 });

function initializeGame() {
	display = document.getElementById("time");
	scoreDisplay = document.getElementById("score");
	statusDisplay = document.getElementById("status");
	playDisplay = document.getElementById("play");
	levelDisplay = document.getElementById("level");
	var canvas = document.getElementById("gl-canvas");
	gl = WebGLUtils.setupWebGL(canvas);
	if (!gl) {
		alert("WebGL is not available");
	}

	gl.viewport(0, 0, 512, 512); // set size of viewport
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // background black
	gl.clear(gl.COLOR_BUFFER_BIT); // allows color

	var timestep = lastFrame;
	player = new Player(gl); // create new player
	player.render(false, false, 0);
	startDemo();
	render(timestep);
}

var keyEnum = {
	UP: 0,
	DOWN: 1,
	LEFT: 2,
	RIGHT: 3
};
var keyArray = new Array(4);
var demo = true;

function keyDown(event) {
  var keyCode = event.keyCode;

  if (keyCode == 32) {
    // SPACE
    clearInterval(1);
    keyUp(evtUp37);
    keyUp(evtUp38);
    keyUp(evtUp40);
    keyUp(evtUp39);
    demo = false;
    event.preventDefault();
  } else if (keyCode == 37) {
    // LEFT
    event.preventDefault();
    keyArray[keyEnum.LEFT] = true;
	// console.log('left');
  } else if (keyCode == 38) {
    // UP
    event.preventDefault();
    keyArray[keyEnum.UP] = true;
	// console.log('up');
  } else if (keyCode == 39) {
    // RIGHT
    event.preventDefault();
    keyArray[keyEnum.RIGHT] = true;
	// console.log('right');
  } else if (keyCode == 40) {
    // DOWN
    event.preventDefault();
    keyArray[keyEnum.DOWN] = true;
	// console.log('down');
  }
}

function keyUp(event) {
  var keyCode = event.keyCode;
  if (keyCode == 32) {
    // SPACE
    clearInterval(gameTimerDemo);
    event.preventDefault();
    startGame();	
  } else if (keyCode == 37) {
    // LEFT
    event.preventDefault();
    keyArray[keyEnum.LEFT] = false;
  } else if (keyCode == 38) {
    // UP
    event.preventDefault();
    keyArray[keyEnum.UP] = false;
  } else if (keyCode == 39) {
    // RIGHT
    event.preventDefault();
    keyArray[keyEnum.RIGHT] = false;
  } else if (keyCode == 40) {
    // DOWN
    event.preventDefault();
    keyArray[keyEnum.DOWN] = false;
  }
}

function render(frameStart) {
	var timestep = frameStart - lastFrame;
	lastFrame = frameStart;
	gl.clear(gl.COLOR_BUFFER_BIT); // allows color
	player.render(
		keyArray[keyEnum.LEFT],
		keyArray[keyEnum.RIGHT],
		keyArray[keyEnum.UP],
		keyArray[keyEnum.DOWN],
		timestep
	);

	for (var i = 0; i < Spikes.length; i++) {
		Spikes[i].render(timestep);
		if (
			Spikes[i].points[Spikes[i].rightBottomMax][1] <=
			player.points[player.leftTopMax][1] &&
			Spikes[i].points[Spikes[i].leftTopMax][1] >=
			player.points[player.rightBottomMax][1] &&
			Spikes[i].points[Spikes[i].rightBottomMax][0] >=
			player.points[player.leftTopMax][0] &&
			Spikes[i].points[Spikes[i].leftTopMax][0] <=
			player.points[player.rightBottomMax][0]
		) {
			score -= Spikes[i].SpikeData.points;
			scoreDisplay.textContent = score;
			Spikes.splice(i, 1);
			i--;
		} else if (Spikes[i].points[Spikes[i].leftTopMax][1] <= -1.0) {
			Spikes.splice(i, 1);
			i--;
		}
	}
	window.requestAnimFrame(render);
}
var gameTimer;

function startTimer(duration, display) {
	var timer = duration,
		minutes,
		seconds;
	gameTimer = setInterval(function () {
		minutes = parseInt(timer / 60, 10);
		seconds = parseInt(timer % 60, 10);

		minutes = minutes < 10 ? "0" + minutes : minutes;
		seconds = seconds < 10 ? "0" + seconds : seconds;

		display.textContent = minutes + ":" + seconds;

		SpawnSpikes(timer);

		if (--timer < 0) {
			timer = 0;
		}
	}, 1000);
}

function startGame() {
	console.log("level >>>>>>" + level);
	if (gameStarted == false) {
		gameStarted = true;
		levelDisplay.textContent = level;
		score = 200;
		scoreDisplay.textContent = score;
		statusDisplay.textContent = "Playing...";
		statusDisplay.style.color = "gray";
		playDisplay.textContent = "";
		startTimer(SetTime, display);
	}
}

function SpawnSpikes(timer) {
	if (timer != 0 && score >= scoreThreshold) {
		for(var i = 0; i < level; i++) {
			Spikes.push(new Spike(gl));
		}
	} else {
		Spikes.splice(0, Spikes.length);
		clearInterval(gameTimer);
		display.textContent = "01:00";
		gameStarted = false;
		if (score >= scoreThreshold) {
			statusDisplay.textContent = "Onto the next level!";
			statusDisplay.style.color = "green";
			playDisplay.textContent = "You've completed level " + level + "! Press SPACE to play level " + (level+1) +  "!";
			level += 1;
			if (level > 10) {
				playDisplay.textContent = "You've completed the game! Press SPACE to play again!";
				statusDisplay.textContent = "YOU WON!";
				level = 1;
			}
		} else {
			statusDisplay.textContent = "YOU LOST!";
			statusDisplay.style.color = "red";
			level = 1;
		}
		
		levelDisplay.textContent = level;
	}
}

function SpawnSpikesDemo(timer) {
	if (timer != 0 && score >= scoreThreshold) {
		if (level == 1) {
			Spikes.push(new Spike(gl));
		} else if (level == 2) {
			Spikes.push(new Spike(gl));
			Spikes.push(new Spike(gl));
			Spikes.push(new Spike(gl));
		} else if (level == 3) {
			Spikes.push(new Spike(gl));
			Spikes.push(new Spike(gl));
			Spikes.push(new Spike(gl));
			Spikes.push(new Spike(gl));
			Spikes.push(new Spike(gl));
		}
	}
}

var gameTimerDemo;
//var keyDemo = [38, 40, 37, 39, 38, 40, 39, 37, 38, 39, 40];
var keyDemo = [37, 38, 39, 40];
var turun = true;
var counterr = 0;
var randKey = 0;
function startDemo() {
  timer = 60;

  gameTimerDemo = setInterval(function() {
    minutes = parseInt(timer / 60, 10);
    seconds = parseInt(timer % 60, 10);

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    SpawnSpikesDemo(timer);
    if (turun) {
		console.log(player.points[player.leftTopMax][0]);//left
		console.log(player.points[player.leftTopMax][1]);//top
		console.log(player.points[player.rightBottomMax][0]);//right
		console.log(player.points[player.rightBottomMax][1]);//bottom
		if (player.points[player.leftTopMax][0] <= -0.9) {
			randKey = keyDemo[2];
			console.log('left');
		  } else if (player.points[player.rightBottomMax][0] >= 0.9){
			randKey = keyDemo[0];
			console.log('right');
		  } else if (player.points[player.leftTopMax][1] >= 0.9){
			randKey = keyDemo[3];
			console.log('top');
		  } else if (player.points[player.rightBottomMax][1] <= -0.9){
			randKey = keyDemo[1];
			console.log('bot');
		  }else{
			randKey = keyDemo[Math.floor(Math.random() * 4)];
			console.log('rand');
		  }
      console.log(randKey);
      var evt = new KeyboardEvent("keydown", { keyCode: randKey });
      keyDown(evt);
      turun = false;
    } else {
		keyUp(evtUp38);
		keyUp(evtUp39);
		keyUp(evtUp40);
		keyUp(evtUp37);
      turun = true;
    }

    if (--timer < 0) {
      timer = 0;
    }
  }, 500);

  // Using the function
  // while (true) simulateKey(38, "down");
}
