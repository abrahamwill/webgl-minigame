var SpikeVShaderID = "vertexShader";


var SpikeData = {
	red: {
		color: "yellow",
		speed: 5,
		size: 3,
		points: 10
	},
	blue: {
		color: "pink",
		speed: 4,
		size: 2,
		points: 15
	},
	white: {
		color: "white",
		speed: 3,
		size: 1.5,
		points: 25
	},
	key: function(n) {
        return this[Object.keys(this)[n]];
    }
};


var minSize = 0.1;

var Spike = function (gl) {
	this.gl = gl;
	this.SpikeID = Math.floor((Math.random() * 3));
	this.SpikeData = SpikeData.key(this.SpikeID);
	this.startx = (Math.random() * (1-(minSize*this.SpikeData.size)+1))-1;
	this.points = [vec2(this.startx,1.01+(minSize*this.SpikeData.size)),vec2(this.startx+(minSize*this.SpikeData.size),1.01+(minSize*this.SpikeData.size)),vec2((this.startx+(minSize*this.SpikeData.size)/2),1.01)];
	this.leftTopMax = 0;
	this.rightBottomMax = 2;
	this.shiftX = 0;
	this.shiftY = 0;
	this.deltaTrans = 0.010*this.SpikeData.speed;
	this.buffer = gl.createBuffer();
	gl.bindBuffer( gl.ARRAY_BUFFER, this.buffer );
	gl.bufferData( gl.ARRAY_BUFFER,	flatten(this.points), gl.STATIC_DRAW );
	this.attachShaders();
}

Spike.prototype.attachShaders = function() {
	var vertexShader = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShader, document.getElementById(SpikeVShaderID).text);
    gl.compileShader(vertexShader);
	
	var fragShader = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragShader, document.getElementById(this.SpikeData.color).text );
	gl.compileShader(fragShader);
	
	this.shaderProgram = gl.createProgram();
	this.gl.attachShader(this.shaderProgram, vertexShader);
    this.gl.attachShader(this.shaderProgram, fragShader);
	this.gl.linkProgram(this.shaderProgram);
}

Spike.prototype.attachVariables = function() {
	var myPosition = this.gl.getAttribLocation(this.shaderProgram, "myPosition");
	this.gl.vertexAttribPointer( myPosition, 2, this.gl.FLOAT, false, 0, 0 );
	this.gl.enableVertexAttribArray( myPosition );
	
	this.xshiftLoc = this.gl.getUniformLocation(this.shaderProgram,"xshift");
	this.yshiftLoc = this.gl.getUniformLocation(this.shaderProgram,"yshift");
}

Spike.prototype.moveX = function(forward) {
	var change = forward ? this.deltaTrans : -this.deltaTrans;
	if (this.points[this.leftTopMax][0] + change >= -1.01 && this.points[this.rightBottomMax][0] + change < 1.01) {
		this.shiftX += change;
		for (var i=0; i<this.points.length;i++) {
		this.points[i][0] = this.points[i][0]+change;
		}
	}
}

Spike.prototype.moveY = function(forward, timediff) {
	var change = forward ? this.deltaTrans : -this.deltaTrans;
	change *= timediff/25;
	this.shiftY += change;
	for (var i=0; i<this.points.length;i++) {
		this.points[i][1] = this.points[i][1]+change;
	}
}

Spike.prototype.render = function(timediff) {
	gl.useProgram(this.shaderProgram);
	gl.bindBuffer( gl.ARRAY_BUFFER, this.buffer );
	this.attachVariables();
	this.moveY(false, timediff);
	this.gl.uniform1f(this.yshiftLoc,this.shiftY);
	this.gl.drawArrays(this.gl.TRIANGLE_FAN, 0, this.points.length);
}